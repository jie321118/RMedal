## 目录结构
##### 1. base 基类
- ActivityBase
- AlertDialogBuilder   Dialog工具类
- BasePresenter
- BaseView
- FragmentBase
- JumpOptions Activity跳转工具类
- TUtil 类转换
##### 2. baserx rxjava 封装
- BasicParamsInterceptor 公共参数拦截器
- DownloadInterceptor   下载参数拦截器
- MyHttpLoggingInterceptor HttpLogging 打印Header
- RxBus RxJava实现的EventBus
- RxManager 管理单个presenter的RxBus的事件和Rxjava相关代码
- RxSchedulers RxJava调度管理
##### 3. listener
- AntiShake 防止重复点击
- LimitQueue 存储一定对象的链表，超出了定义的范围就删除第一个对象
- OnDelayListener 延时任务封装回调
- OnNoFastClickListener 重复点击的监听器

##### 4.  常用封装工具
- ACache 本地文件缓存工具类
- ActivityTManager Activity操作相关工具类
- CaptchaTime 验证码倒计时
- ClipboardUtils 剪贴板相关工具
- DataUtils 数据处理相关
- DeviceUtils 手机硬件信息设备工具类
- EncodeUtils 编码解码相关工具类
- EncryptUtils 加密解密相关的工具类
- FileUtils SD卡操作工具
- ImageUtils 图像工具类
- IntentUtils Intent相关
- JsonUtils JSON解析二次封装
- KeyboardUtils 键盘相关
- NavigationBarResetUtil 底部导航栏遮挡布局
- NetWorkUtils 网络相关
- PermissionsUtils 权限请求操作工具类
- PhotoUtils 相机相关工具
- SoftHideKeyBoardUtils 键盘档住输入框
- SPUtils SharedPreferences工具类
- StatusBarUtils 状态栏工具
- SystemUiVisibilityUtil 显示或隐藏状态栏
- TimeUtils 时间相关
- ToastTool Toast的封装 normal warning info success

- Note
- LogUtils Log管理
- VnUtils 常用工具类

##### 5. widget
- html WebView重新封装
- imageload 图片加载工具类 使用glide框架封装
- overscroll 仿ios回弹
- progressing 加载动画 SpriteFactory控制不同效果
- sidebar 波浪侧边栏(字母快速导航)
- swipemenu Item侧滑删除菜单
- ticker 数字增长动画
- ClearableEditText 带有删除按钮的EditText
- CornerImageView 圆形圆角图片
- InnerPagerAdapter  TabLayout 配套的Adapter
- ItemDecoration  RecyclerView线
- JustifyAlignTextView  文字两端对齐
- KeyboardLayout 键盘档住输入框--适配键盘高度变化
- LabelsView 自定义标签列表
- PasswordEditText 带有显示密码按钮的EditText
- RunTextView 跑马灯效果
- ShoppingView 商品数量加减控件
- TextAutoZoom 文字根据布局大小自动缩放效果
- TextViewVertical 单行文字上下滚动
- TextViewVerticalMore 多行文字上下滚动
- TitleView 自定义标题控件
- TouchImageView 图片的缩放，双击放大缩小，多点触控的功能
- VerificationCodeView 动态生成验证码
- ViewPagerFixed  修复v4 的ViewPager点击会崩溃


